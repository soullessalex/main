import datetime

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.sql import func


Base = declarative_base()


class CommonMixture:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.now)


class Technic(CommonMixture, Base):
    __tablename__ = "technics"

    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)


class TechnicPrice(CommonMixture, Base):
    __tablename__ = "technics_prices"

    technic_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Technic.__tablename__}.id"), nullable=False)
    price = sa.Column(sa.Float, sa.CheckConstraint("price>=0"), nullable=False)


class Deal(CommonMixture, Base):
    __tablename__ = "deals"

    technic_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Technic.__tablename__}.id"), nullable=False)
    amount = sa.Column(sa.Float, nullable=False)
    cost = sa.Column(sa.Float, nullable=False)
    cost = TechnicPrice.price * amount


def add(function):
    def wrapper(self, *args, **kwargs):
        record = function(self, *args, **kwargs)
        self._session.add(record)
        self._session.commit()
        return record
    return wrapper


def delete(function):
    def wrapper(self, *args, **kwargs):
        record = function(self, *args, **kwargs)
        self._session.delete(record)
        self._session.commit()
    return wrapper


class Role:
    def __init__(self, engine, name, host=None, port=None, username=None, password=None, **params):
        dsn = f"{engine}://"
        if username is not None:
            dsn += username
        if password is not None:
            dsn += f":{password}"
        if username is not None:
            dsn += "@"
        if host is not None:
            dsn += f"{host}"
        if port is not None:
            dsn += f":{port}"
        dsn += f"/{name}"
        if params:
            dsn += "?"
        dsn += "&".join(f"{key}={value}" for key, value in params.items())

        self._engine = sa.create_engine(dsn)
        Base.metadata.create_all(self._engine)
        self._session = sessionmaker(bind=self._engine)()


class Dealer(Role):
    def get_technic(self, name):
        return self._session.query(Technic).filter(Technic.name == name).first()

    def get_amount_technic(self, technic):
        return self._session.query(func.sum(Deal.amount).label("amount")).filter(
            Deal.technic_id == technic.id,
        ).first()[0]

    def get_technicprice(self, technic, price):
        return self._session.query(func.sum(TechnicPrice.price).label("price")).filter(
            TechnicPrice.technic_id == technic.id,
        ).first()[0]

    @add
    def deal(self, name, amount):
        technic = self.get_technic(name)
        if technic is None:
            raise Exception("Has no technic with name '%s'" % name)
        
        has_amount = self.get_amount_technic(technic)
        if amount > has_amount:
            raise Exception("Has no %f amount for sell '%s'. Current amount: %f" % (
                amount, name, has_amount,
            ))

        return Deal(technic_id=technic.id, amount=-amount)


class AdminRole(Dealer):
    @add
    def operations(self, name, amount, price):
        technic = self.get_technic(name)
        if technic is None:
            technic = Technic(name=name)
            self._session.add(technic)
            self._session.commit()

        technicprice = self.get_technicprice(technic, price)
        if technicprice is None:
            technicprice = TechnicPrice(technic_id=technic.id, price=price)
            self._session.add(technicprice)
            self._session.commit()
        
        return Deal(technic_id=technic.id, amount=amount)


admin = AdminRole(engine="sqlite", name="technic.db")
dealer = Dealer(engine="sqlite", name="technic.db")
admin.operations(name="Самосвал", amount=20, price=5000)
admin.deal(name="Самосвал", amount=10)
admin.operations(name="Погрузчик", amount=40, price=6000)
dealer.deal(name="Погрузчик", amount=35)
admin.operations(name="Укладчик", amount=30, price=4000)
dealer.deal(name="Укладчик", amount=20)