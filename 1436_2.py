x = int(input())
lst = list(map(int, (input().split())))


def insertion_sort(a):
    for i in range(1, len(a)):
        tmp = a[i]
        j = i - 1
        flag=0
        while j >= 0 and a[j]>tmp:
            a[j + 1] = a[j]
            j -= 1
            flag += 1
        a[j + 1] = tmp
        if flag!=0:
            print(' '.join(map(str, a)))
        

insertion_sort(lst)