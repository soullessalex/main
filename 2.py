NK = list(map(int, (input().split())))
N = list(map(int, (input().split())))
N.sort()
K = list(map(int, (input().split())))


def nearest(lst, target):
    return min(lst, key=lambda x: abs(x-target))

for k in K:
    print(nearest(N, k))
