x = int(input())
lst = list(map(int, (input().split())))


# iterations = 0
for index in range(1, len(lst)):
    iterations = 0
    while index > 0 and lst[index] < lst[index-1]:
        iterations += 1
        lst[index], lst[index-1] = lst[index-1], lst[index]
        index -= 1
    if iterations != 0:
        print(' '.join(map(str, lst)))
