x = int(input())
lst = list(map(int, (input().split())))

iterations = 0
for i in range(1, len(lst)):
    for j in range(len(lst) - i):
        if lst[j] > lst[j+1]:
            lst[j+1], lst[j] = lst[j], lst[j+1]
            iterations += 1

print(iterations)




